#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cctype>
#include <ctime>
#include <cstdlib>
#include <conio.h>
#include <Windows.h>
#include <vector>

void main()
{
	srand(time(0));
	int map[100][100], goalmap[100][100], boulder_counter, boulderX, boulderY, doorX, doorY, goalX, goalY, playerX, playerY, difficulty = 2;
	int markX[5], markY[5], markCounter, mark_amount, mark_checked, stage_number = 0, j[5], completed_stage = 0, completed_staged[20];
	bool collision, collisionBoul, stage_load, restart_bool = false;
	bool boul[99];
	bool pause;
	bool gameover = false;
	bool moveup = false,movedown = false,moveleft = false,moveright = false,moving = false;
	int movementspeed = 4;
	int blocksize = 32;
	int targetpos;

	sf::Vector2i mapSize;
	
	pause = false;

	stage_load = true;

	mapSize.x = 17;
	mapSize.y = 19;
	
	int timer = 300, timer_minute, timer_second;
	sf::Font textFont;
	sf::String sentence, notif_sentence, stage_sentence;
	sf::Text text, notif_text, restart, stage_text, pause_text, gameover_text, congratulations_text;
	std::ostringstream convert_minute, convert_second, convert_stage;

	timer_minute = timer / 60;
	timer_second = timer - timer_minute * 60;

	convert_minute << timer_minute;
	convert_second << timer_second;

	if(timer_second < 10)
	{
		sentence = "Time : " + convert_minute.str() + ":0" + convert_second.str();
	}
	else
	{
		sentence = "Time : " + convert_minute.str() + ":" + convert_second.str();
	}
	textFont.loadFromFile("Lumberjack.otf");
	text.setFont(textFont);
	text.setCharacterSize(32);
	text.setColor(sf::Color(255,255,255,255));
	text.setPosition(608, 26);
	text.setString(sentence);

	restart.setFont(textFont);
	restart.setCharacterSize(32);
	restart.setColor(sf::Color(255,255,255,255));
	restart.setPosition(620, 218);
	restart.setString("Restart");

	stage_text.setFont(textFont);
	stage_text.setCharacterSize(32);
	stage_text.setColor(sf::Color(255,255,255,255));
	stage_text.setPosition(620, 122);

	pause_text.setFont(textFont);
	pause_text.setCharacterSize(32);
	pause_text.setColor(sf::Color(255,255,255,255));
	pause_text.setPosition(32, 32);
	pause_text.setString("Paused");

	gameover_text.setFont(textFont);
	gameover_text.setCharacterSize(64);
	gameover_text.setColor(sf::Color(255,255,255,255));
	gameover_text.setPosition(96,256);
	gameover_text.setString("Game Over!");

	congratulations_text.setFont(textFont);
	congratulations_text.setCharacterSize(64);
	congratulations_text.setColor(sf::Color(255,255,255,255));
	congratulations_text.setPosition(64, 256);
	congratulations_text.setString("Congratulations!");

	notif_sentence = "Stage 1 \nCompleted!";
	notif_text.setString(notif_sentence);
	notif_text.setPosition(550, 40);
	notif_text.setFont(textFont);
	notif_text.setCharacterSize(40);
	notif_text.setColor(sf::Color(255,255,255,255));

	sf::Vector2i screenDimension(800, 600);

	enum pDirection {Down, Left, Right, Up};
	
	float frameCounter,switchFrame,frameSpeed;

	frameCounter = 0;
	switchFrame = 200;
	frameSpeed = 800;

	sf::Clock Clock, Clock2;
	sf::Time Time, Time2;

	sf::Vector2i Source;
	Source.x = 1;
	Source.y = Down;

	std::vector<sf::VideoMode> availableVideoModes;

	availableVideoModes = sf::VideoMode::getFullscreenModes();
	std::cout << availableVideoModes[5].width;

	sf::RenderWindow Window(sf::VideoMode(screenDimension.x, screenDimension.y),"",sf::Style::Fullscreen);
	Window.setTitle("Boulders");

	sf::Texture pTexture, bTexture, gTexture, boulTexture, wallTexture, markTexture, doorTexture, goalTexture, doorOpenedTexture, menuBGTexture, flowerBedTexture;
	sf::Texture riverUpTexture, riverDownTexture, riverURCTexture, riverBLCTexture, decorationHouse1Texture;
	sf::Sprite pSprite;

	pTexture.loadFromFile("p1.png");
	pSprite.setTexture(pTexture);
	pSprite.setPosition(32, 32);

	sf::RectangleShape gTile(sf::Vector2f(32, 32));
	
	gTexture.loadFromFile("grass.png");
	gTile.setTexture(&gTexture);

	sf::RectangleShape wall(sf::Vector2f(32, 32));

	wallTexture.loadFromFile("wstump.png");
	wall.setTexture(&wallTexture);
	
	boulTexture.loadFromFile("boulder.png");

	sf::RectangleShape mark(sf::Vector2f(32, 32));
	markTexture.loadFromFile("mark.png");
	mark.setTexture(&markTexture);

	sf::RectangleShape door(sf::Vector2f(32, 32)), doorMedium, doorHard;
	doorTexture.loadFromFile("doorClosed.png");
	door.setTexture(&doorTexture);

	doorMedium.setSize(sf::Vector2f(32, 32));
	doorMedium.setTexture(&doorTexture);
	
	doorHard.setSize(sf::Vector2f(32, 32));
	doorHard.setTexture(&doorTexture);

	sf::RectangleShape doorOpened(sf::Vector2f(32, 32));
	doorOpenedTexture.loadFromFile("doorOpened.png");
	doorOpened.setTexture(&doorOpenedTexture);

	sf::RectangleShape goal(sf::Vector2f(32, 32));
	
	goalTexture.loadFromFile("snail.png");
	goal.setTexture(&goalTexture);

	sf::RectangleShape Boulder[100];
	
	for(int i = 0; i < 100; i++)
	{
		Boulder[i].setSize(sf::Vector2f(32,32));
		Boulder[i].setTexture(&boulTexture);
	}

	sf::RectangleShape riverUp(sf::Vector2f(32, 32));

	riverUpTexture.loadFromFile("water1.png");
	riverUp.setTexture(&riverUpTexture);

	sf::RectangleShape riverDown(sf::Vector2f(32, 32));

	riverDownTexture.loadFromFile("water2.png");
	riverDown.setTexture(&riverDownTexture);

	sf::RectangleShape riverURC(sf::Vector2f(32, 32));

	riverURCTexture.loadFromFile("water3.png");
	riverURC.setTexture(&riverURCTexture);

	sf::RectangleShape riverBLC(sf::Vector2f(32, 32));

	riverBLCTexture.loadFromFile("water4.png");
	riverBLC.setTexture(&riverBLCTexture);
	
	sf::RectangleShape decorationHouse1(sf::Vector2f(256, 192));

	decorationHouse1Texture.loadFromFile("decorationHouse1.png");
	decorationHouse1.setTexture(&decorationHouse1Texture);
	decorationHouse1.setPosition(sf::Vector2f(544, 412));

	sf::RectangleShape menuBG1(sf::Vector2f(256, 96)), menuBG2(sf::Vector2f(256, 96)), menuBG3(sf::Vector2f(256, 96)), flowerBed(sf::Vector2f(256, 128));

	menuBGTexture.loadFromFile("menubg.png");
	menuBG1.setTexture(&menuBGTexture);
	menuBG1.setPosition(sf::Vector2f(544, 0));
	menuBG2.setTexture(&menuBGTexture);
	menuBG2.setPosition(sf::Vector2f(544, 96));
	menuBG3.setTexture(&menuBGTexture);
	menuBG3.setPosition(sf::Vector2f(544, 192));

	flowerBedTexture.loadFromFile("flowerbed.png");
	flowerBed.setTexture(&flowerBedTexture);
	flowerBed.setPosition(sf::Vector2f(544, 288));

	sf::Time InputCD;
	sf::Clock CDClock;

	Window.setFramerateLimit(60);
	Window.setVerticalSyncEnabled(true);

    while (Window.isOpen())
    {
		Time = Clock.getElapsedTime();
		InputCD = CDClock.getElapsedTime();
		int boul_push;
        sf::Event Event;
		if(stage_load == true)
		{
			doorMedium.setPosition(-1000, -1000);
			doorHard.setPosition(-1000, -1000);
			convert_stage.str("");
			convert_stage.clear();
			convert_stage << int(completed_stage);

			stage_sentence = "Stage : " + convert_stage.str();
			stage_text.setString(stage_sentence);

			moving = false;
			moveup = false;
			movedown = false;
			moveleft = false;
			moveright = false;

			boulder_counter = 0;
			boul_push = -1;

			doorX = -999;
			doorY = -999;
			mark_checked = 0;
			mark_amount = 0;
			doorOpened.setPosition(sf::Vector2f(-500,-500));
			
			if(restart_bool == false)
			{
				if(completed_stage != 0)
				{
					bool reroll = true;
					while(reroll == true)
					{
						if(completed_stage <= 2)
						{
							stage_number = rand()%4+1;
						}
						else
						{
							stage_number = rand()%5+12;
						}

						for(int i = 0; i < completed_stage; i++)
						{
							if(stage_number == completed_staged[i])
							{
								reroll = true;
								break;
							}
							else
							{
								reroll = false;
							}
						}
					}
				}
			}

			if (difficulty == 1)
			{
				if (completed_stage == 4)
				{
					stage_number = 99;
				}
			}
			else if (difficulty == 2)
			{
				if (completed_stage == 5)
				{
					stage_number = 99;
				}
			}
			else if (difficulty == 3)
			{
				if (completed_stage == 6)
				{
					stage_number = 99;
				}
			}
			
			std::ifstream openmap("mapcomp.txt");

			if(openmap.is_open())
			{
				int stage_num_scan;
				std::string trash;
				openmap >> stage_num_scan;
				while(stage_num_scan != stage_number)
				{
					for(int i = 0; i < mapSize.y+1; i++)
					{
						std::getline(openmap,trash);
					}
					openmap >> stage_num_scan;
				}

				for(int i = 0; i < mapSize.y; i++)
				{
					for(int j = 0; j < mapSize.x; j++)
					{
						openmap >> map[i][j];
						if(map[i][j] == 1)
						{
							mark_amount++;
						}
						else if (map[i][j] == 3)
						{
							goalY = i;
							goalX = j;
							doorY = i;
							doorX = j;
						}
						else if(map[i][j] == 5)
						{
							pSprite.setPosition(j * 32,i * 32);
						}
						else if(map[i][j] == 6)
						{
							Boulder[boulder_counter].setPosition(j * 32, i * 32);
							boulder_counter++;
						}
						else if (map[i][j] == 11)
						{
							doorMedium.setPosition(j * 32, i * 32);
						}
						else if (map[i][j] == 12)
						{
							doorHard.setPosition(j * 32, i * 32);
						}
					}
				}

				for(int i = 0; i < mapSize.y; i++)
				{
					for(int j = 0; j < mapSize.x; j++)
					{
						goalmap[i][j] = map[i][j];
					}
				}
			}
			map[int(doorMedium.getPosition().y / 32)][int(doorMedium.getPosition().x / 32)] = 3;
			map[int(doorHard.getPosition().y / 32)][int(doorHard.getPosition().x / 32)] = 3;
			openmap.close();
			stage_load = false;
		}

		while (Window.pollEvent(Event))
        {
            if (Event.type == sf::Event::Closed)
			{
				Window.close();
			}	
			if(Event.type == sf::Event::KeyReleased)
			{
				if(Event.key.code == sf::Keyboard::C)
				{
					if(pause == false)
					{
						std::cout << mark_checked << "\n";
						pause = true;
					}
					else
					{
						pause = false;
					}
				}
				if(Event.key.code == sf::Keyboard::R)
				{
					stage_load = true;
					restart_bool = true;
				}
				if(Event.key.code == sf::Keyboard::Escape)
				{
					Window.close();
				}
			}
			if(Event.type == sf::Event::MouseButtonReleased)
			{
				if(Event.mouseButton.button == sf::Mouse::Left && Event.mouseButton.x >= 544 && Event.mouseButton.x <= 800 && Event.mouseButton.y >= 192 && Event.mouseButton.y <= 288)
				{
					stage_load = true;
					restart_bool = true;
				}
			}
		}
		Window.draw(menuBG1);
		Window.draw(menuBG2);
		Window.draw(menuBG3);
		Window.draw(flowerBed);
		if(pause == false && gameover == false)
		{
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			{
				/*if(InputCD.asSeconds() >= 0.12)
				{*/
				if(moving == false)
				{
					CDClock.restart();
					if(map[int(pSprite.getPosition().y/32) - 1][int(pSprite.getPosition().x/32)] != 2 && map[int(pSprite.getPosition().y/32) - 1][int(pSprite.getPosition().x/32)] != 3)
					{
						if(map[int(pSprite.getPosition().y/32) - 1][int(pSprite.getPosition().x/32)] == 6)
						{
							if(map[int(pSprite.getPosition().y/32) - 2][int(pSprite.getPosition().x/32)] != 2 && map[int(pSprite.getPosition().y/32) - 2][int(pSprite.getPosition().x/32)] != 6 && map[int(pSprite.getPosition().y/32) - 2][int(pSprite.getPosition().x/32)] != 3)
							{
								map[int(pSprite.getPosition().y/32) - 2][int(pSprite.getPosition().x/32)] = 6;
								map[int(pSprite.getPosition().y/32) - 1][int(pSprite.getPosition().x/32)] = 5;
								moveup = true;
								moving = true;
								targetpos = pSprite.getPosition().y - blocksize;
								for(int i = 0; i < boulder_counter; i++)
								{
									if(map[int(Boulder[i].getPosition().y/32) + 1][int(Boulder[i].getPosition().x/32)] == 5)
									{
										boul_push = i;
										break;
									}
								}
								if(goalmap[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] == 1)
								{
									map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] = 1;
								}
								else
								{
									map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] = 0;
								}
							}
						}
						else
						{
							map[int(pSprite.getPosition().y/32) - 1][int(pSprite.getPosition().x/32)] = 5;
							moveup = true;
							moving = true;
							targetpos = pSprite.getPosition().y - blocksize;
							boul_push = -1;
							if(goalmap[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] == 1)
							{
								map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] = 1;
							}
							else
							{
								map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] = 0;
							}
						}
					}
				}
				Source.y = Up;
				frameCounter = frameCounter + frameSpeed * Clock.restart().asSeconds();
				if(frameCounter >= switchFrame)
				{
					frameCounter = 0;
					Source.x++;
					if(Source.x * 32 >= pTexture.getSize().x)
					{
						Source.x = 0;
					}
				}
			}
			else if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			{
				/*if(InputCD.asSeconds() >= 0.12)
				{*/
				if(moving == false)
				{
					CDClock.restart();
					if(map[int(pSprite.getPosition().y/32) + 1][int(pSprite.getPosition().x/32)] != 2 && map[int(pSprite.getPosition().y/32) + 1][int(pSprite.getPosition().x/32)] != 3)
					{
						if(map[int(pSprite.getPosition().y/32) + 1][int(pSprite.getPosition().x/32)] == 6)
						{
							if(map[int(pSprite.getPosition().y/32) + 2][int(pSprite.getPosition().x/32)] != 2 && map[int(pSprite.getPosition().y/32) + 2][int(pSprite.getPosition().x/32)] != 6 && map[int(pSprite.getPosition().y/32) + 2][int(pSprite.getPosition().x/32)] != 3)
							{
								map[int(pSprite.getPosition().y/32) + 2][int(pSprite.getPosition().x/32)] = 6;
								map[int(pSprite.getPosition().y/32) + 1][int(pSprite.getPosition().x/32)] = 5;
								movedown = true;
								moving = true;
								targetpos = pSprite.getPosition().y + blocksize;
								for(int i = 0; i < boulder_counter; i++)
								{
									if(map[int(Boulder[i].getPosition().y/32) - 1][int(Boulder[i].getPosition().x/32)] == 5)
									{
										boul_push = i;
										break;
									}
								}
								if(goalmap[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] == 1)
								{
									map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] = 1;
								}
								else
								{
									map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] = 0;
								}
							}
						}
						else
						{
							map[int(pSprite.getPosition().y/32) + 1][int(pSprite.getPosition().x/32)] = 5;
							movedown = true;
							moving = true;
							targetpos = pSprite.getPosition().y + blocksize;
							boul_push = -1;
							if(goalmap[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] == 1)
							{
								map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] = 1;
							}
							else
							{
								map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] = 0;
							}
						}
					}
				}
				Source.y = Down;
				frameCounter = frameCounter + frameSpeed * Clock.restart().asSeconds();
				if(frameCounter >= switchFrame)
				{
					frameCounter = 0;
					Source.x++;
					if(Source.x * 32 >= pTexture.getSize().x)
					{
						Source.x = 0;
					}
				}
			}
			else if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			{
				/*if(InputCD.asSeconds() >= 0.12)
				{*/
				if(moving == false)
				{
					CDClock.restart();
					if(map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) - 1] != 2 && map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) - 1] != 3)
					{
						if(map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) - 1] == 6)
						{
							if(map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) - 2] != 2 && map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) - 2] != 6 && map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) - 2] != 3)
							{
								map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) - 2] = 6;
								map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) - 1] = 5;
								moveleft = true;
								moving = true;
								targetpos = pSprite.getPosition().x - blocksize;
								for(int i = 0; i < boulder_counter; i++)
								{
									if(map[int(Boulder[i].getPosition().y/32)][int(Boulder[i].getPosition().x/32) + 1] == 5)
									{
										boul_push = i;
										break;
									}
								}
								if(goalmap[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] == 1)
								{
									map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] = 1;
								}
								else
								{
									map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] = 0;
								}
							}
						}
						else
						{
							map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) - 1] = 5;
							moveleft = true;
							moving = true;
							targetpos = pSprite.getPosition().x - blocksize;
							boul_push = -1;
							if(goalmap[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] == 1)
							{
								map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] = 1;
							}
							else
							{
								map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] = 0;
							}
						}
					}
				}
				Source.y = Left;
				frameCounter = frameCounter + frameSpeed * Clock.restart().asSeconds();
				if(frameCounter >= switchFrame)
				{
					frameCounter = 0;
					Source.x++;
					if(Source.x * 32 >= pTexture.getSize().x)
					{
						Source.x = 0;
					}
				}
			}
			else if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			{
				/*if(InputCD.asSeconds() >= 0.12)
				{*/
				if(moving == false)
				{
					CDClock.restart();
					if(map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) + 1] != 2 && map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) + 1] != 3)
					{
						if(map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) + 1] == 6)
						{
							if(map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) + 2] != 2 && map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) + 2] != 6 && map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) + 2] != 3)
							{
								map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) + 2] = 6;
								map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) + 1] = 5;
								moveright = true;
								moving = true;
								targetpos = pSprite.getPosition().x + blocksize;
								for(int i = 0; i < boulder_counter; i++)
								{
									if(map[int(Boulder[i].getPosition().y/32)][int(Boulder[i].getPosition().x/32) - 1] == 5)
									{
										boul_push = i;
										break;
									}
								}
								if(goalmap[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] == 1)
								{
									map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] = 1;
								}
								else
								{
									map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] = 0;
								}
							}
						}
						else
						{
							map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32) + 1] = 5;
							moveright = true;
							moving = true;
							targetpos = pSprite.getPosition().x + blocksize;
							boul_push = -1;
							if(goalmap[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] == 1)
							{
								map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] = 1;
							}
							else
							{
								map[int(pSprite.getPosition().y/32)][int(pSprite.getPosition().x/32)] = 0;
							}
						}
					}
				}
				Source.y = Right;
				frameCounter = frameCounter + frameSpeed * Clock.restart().asSeconds();
				if(frameCounter >= switchFrame)
				{
					frameCounter = 0;
					Source.x++;
					if(Source.x * 32 >= pTexture.getSize().x)
					{
						Source.x = 0;
					}
				}
			}
		}
		
		Clock.restart();

		if(pause == false && gameover == false)
		{
			Time2 = Clock2.getElapsedTime();
			if(stage_number == 0 || stage_number == 99)
			{
				Clock2.restart();
			}
			else
			{
				if(Time2.asSeconds() >= 1)
				{
					Clock2.restart();
					convert_minute.str("");
					convert_second.str("");
					convert_minute.clear();
					convert_second.clear();

					if(timer > 0)
					{
						timer--;
					}

					timer_minute = timer / 60;
					timer_second = timer - timer_minute * 60;

					convert_minute << timer_minute;
					convert_second << timer_second;

					if(timer_second < 10)
					{
						sentence = "Time : " + convert_minute.str() + ":0" + convert_second.str();
					}
					else
					{
						sentence = "Time : " + convert_minute.str() + ":" + convert_second.str();
					}
					text.setString(sentence);
				}
			}
		}

		if(timer == 0)
		{
			gameover = true;
		}

		pSprite.setTextureRect(sf::IntRect(Source.x * 32, Source.y * 32, 32, 32));
		
		int check_temp = 0;
		for(int j = 0;j < mapSize.y; j++)
		{
			for(int i = 0;i < mapSize.x; i++)
			{
				if(map[j][i] == 0)
				{
					gTile.setPosition(i * 32, j * 32);
					Window.draw(gTile);
				}
				else if(map[j][i] == 2)
				{
					wall.setPosition(i * 32, j * 32);
					Window.draw(wall);
				}
				else if(map[j][i] == 1)
				{
					mark.setPosition(i * 32, j * 32);
					Window.draw(mark);
				}
				else if(map[j][i] == 3)
				{
					door.setPosition(i * 32, j * 32);
					Window.draw(door);
				}
				else if(map[j][i] == 4)
				{
					doorOpened.setPosition(i * 32, j * 32);
					Window.draw(doorOpened);
				}
				else if(map[j][i] == 5)
				{
					if(goalmap[j][i] == 1)
					{
						mark.setPosition(i * 32, j * 32);
						Window.draw(mark);
					}
					else
					{
						gTile.setPosition(i * 32, j * 32);
						Window.draw(gTile);
					}
				}
				else if(map[j][i] == 6)
				{
					if(goalmap[j][i] == 1)
					{
						mark.setPosition(i * 32, j * 32);
						Window.draw(mark);
					}
					else
					{
						gTile.setPosition(i * 32, j * 32);
						Window.draw(gTile);
					}
				}
				else if(map[j][i] == 7)
				{
					riverUp.setPosition(i * 32, j * 32);
					Window.draw(riverUp);
				}
				else if(map[j][i] == 8)
				{
					riverDown.setPosition(i * 32, j * 32);
					Window.draw(riverDown);
				}
				else if(map[j][i] == 9)
				{
					riverURC.setPosition(i * 32, j * 32);
					Window.draw(riverURC);
				}
				else if(map[j][i] == 10)
				{
					riverBLC.setPosition(i * 32, j * 32);
					Window.draw(riverBLC);
				}

				if(goalmap[j][i] == 1)
				{
					if(map[j][i] == 6)
					{
						check_temp++;
					}
				}
			}
		}

		if(moving == true)
		{
			if(moveup == true)
			{
				if(pSprite.getPosition().y <= targetpos)
				{
					moveup = false;
					moving = false;
				}
				else
				{
					pSprite.setPosition(pSprite.getPosition().x, pSprite.getPosition().y - movementspeed);
					if(boul_push >= 0)
					{
						Boulder[boul_push].setPosition(Boulder[boul_push].getPosition().x, Boulder[boul_push].getPosition().y - movementspeed);
					}
				}
			}
			else if(movedown == true)
			{
				if(pSprite.getPosition().y >= targetpos)
				{
					movedown = false;
					moving = false;
				}
				else
				{
					pSprite.setPosition(pSprite.getPosition().x, pSprite.getPosition().y + movementspeed);
					if(boul_push >= 0)
					{
						Boulder[boul_push].setPosition(Boulder[boul_push].getPosition().x, Boulder[boul_push].getPosition().y + movementspeed);
					}
				}
			}
			else if(moveleft == true)
			{
				if(pSprite.getPosition().x <= targetpos)
				{
					moveleft = false;
					moving = false;
				}
				else
				{
					pSprite.setPosition(pSprite.getPosition().x - movementspeed, pSprite.getPosition().y);
					if(boul_push >= 0)
					{
						Boulder[boul_push].setPosition(Boulder[boul_push].getPosition().x - movementspeed, Boulder[boul_push].getPosition().y);
					}
				}
			}
			else if(moveright == true)
			{
				if(pSprite.getPosition().x >= targetpos)
				{
					moveright = false;
					moving = false;
				}
				else
				{
					pSprite.setPosition(pSprite.getPosition().x + movementspeed, pSprite.getPosition().y);
					if(boul_push >= 0)
					{
						Boulder[boul_push].setPosition(Boulder[boul_push].getPosition().x + movementspeed, Boulder[boul_push].getPosition().y);
					}
				}
			}
		}

		mark_checked = check_temp;
		if(mark_checked == mark_amount)
		{
			map[goalY][goalX] = 4;
			map[int(doorMedium.getPosition().y / 32)][int(doorMedium.getPosition().x / 32)] = 4;
			map[int(doorHard.getPosition().y / 32)][int(doorHard.getPosition().x / 32)] = 4;
		}
		else
		{
			map[goalY][goalX] = 3;
			map[int(doorMedium.getPosition().y / 32)][int(doorMedium.getPosition().x / 32)] = 3;
			map[int(doorHard.getPosition().y / 32)][int(doorHard.getPosition().x / 32)] = 3;
		}
		
		if(pSprite.getPosition().x == doorX * 32 && pSprite.getPosition().y == doorY * 32)
		{
			completed_staged[completed_stage] = stage_number;
			completed_stage++;
			stage_load = true;
			restart_bool = false;
		}
		else if (pSprite.getPosition().x == doorMedium.getPosition().x && pSprite.getPosition().y == doorMedium.getPosition().y)
		{
			difficulty = 2;
			completed_staged[completed_stage] = stage_number;
			completed_stage++;
			stage_load = true;
			restart_bool = false;
		}
		else if (pSprite.getPosition().x == doorHard.getPosition().x && pSprite.getPosition().y == doorHard.getPosition().y)
		{
			difficulty = 3;
			completed_staged[completed_stage] = stage_number;
			completed_stage++;
			stage_load = true;
			restart_bool = false;
		}

		Window.draw(text);

		for(int i = 0; i < boulder_counter; i++)
		{
			Window.draw(Boulder[i]);
		}
		Window.draw(stage_text);
		Window.draw(restart);
		Window.draw(decorationHouse1);
        Window.draw(pSprite);
		if(pause == true)
		{
			Window.draw(pause_text);
		}
		if(gameover == true)
		{
			Window.draw(gameover_text);
		}
        Window.display();
		Window.clear();

    }
	
}


//Backup Codes

//collisionCheckUp
						/*collisionDeciderY1 = (pSprite.getPosition().y / 32);
						collisionDeciderX1 = pSprite.getPosition().x / 32;
						collisionCalcX = pSprite.getPosition().x - floor(pSprite.getPosition().x / 32) * 32;
						if(map[collisionDeciderX1][collisionDeciderY1] == 2)
						{
							if(collisionCalcX >= 20)
							{
								collisionDeciderX1 = ceil(pSprite.getPosition().x / 32);
							}
							else
							{
								collisionDeciderX1 = floor(pSprite.getPosition().x / 32);
							}
						}
						else
						{
							if(collisionCalcX >= 4)
							{
								collisionDeciderX1 = ceil(pSprite.getPosition().x / 32);
							}
							else
							{
								collisionDeciderX1 = floor(pSprite.getPosition().x / 32);
							}
						}
			
						if(map[collisionDeciderX1][collisionDeciderY1] == 2)
						{
							if((pSprite.getPosition().y - ((collisionDeciderY1 + 1) * 32)) <= 0.1 || (pSprite.getPosition().y - ((collisionDeciderY1 + 1) * 32)) <= 32)
							{
							}
							else
							{
								pSprite.move(0, -100 * Time.asSeconds());
							}
						}
						else
						{
							pSprite.move(0, -100 * Time.asSeconds());
						}*/

//collisionCheckDown
						/*collisionDeciderY1 = (pSprite.getPosition().y / 32) + 1;
						collisionDeciderX1 = pSprite.getPosition().x / 32;
						collisionCalcX = pSprite.getPosition().x - floor(pSprite.getPosition().x / 32) * 32;
						if(map[collisionDeciderX1][collisionDeciderY1] == 2)
						{
							if(collisionCalcX >= 20)
							{
								collisionDeciderX1 = ceil(pSprite.getPosition().x / 32);
							}
							else
							{
								collisionDeciderX1 = floor(pSprite.getPosition().x / 32);
							}
						}
						else
						{
							if(collisionCalcX >= 4)
							{
								collisionDeciderX1 = ceil(pSprite.getPosition().x / 32);
							}
							else
							{
								collisionDeciderX1 = floor(pSprite.getPosition().x / 32);
							}
						}

						if(map[collisionDeciderX1][collisionDeciderY1] == 2)
						{
							if((pSprite.getPosition().y - ((collisionDeciderY1 - 1) * 32)) <= 0.1 || (pSprite.getPosition().y - ((collisionDeciderY1 - 1) * 32)) <= 32 )
							{
							}
							else
							{
								pSprite.move(0, 100 * Time.asSeconds());
							}
						}
						else
						{
							pSprite.move(0, 100 * Time.asSeconds());
						}*/

//collisionCheckLeft

						/*collisionDeciderX1 = (pSprite.getPosition().x / 32);
						collisionDeciderY1 = pSprite.getPosition().y / 32;
						collisionCalcY = pSprite.getPosition().y - floor(pSprite.getPosition().y / 32) * 32;
						if(map[collisionDeciderX1][collisionDeciderY1] == 2)
						{
							if(collisionCalcY >= 20)
							{
								collisionDeciderY1 = ceil(pSprite.getPosition().y / 32);
							}
							else
							{
								collisionDeciderY1 = floor(pSprite.getPosition().y / 32);
							}
						}
						else
						{
							if(collisionCalcY >= 4)
							{
								collisionDeciderY1 = ceil(pSprite.getPosition().y / 32);
							}
							else
							{
								collisionDeciderY1 = floor(pSprite.getPosition().y / 32);
							}
						}
			
						if(map[collisionDeciderX1][collisionDeciderY1] == 2)
						{
							if((pSprite.getPosition().x - ((collisionDeciderX1 + 1) * 32)) <= 0.1 || (pSprite.getPosition().x - ((collisionDeciderX1 + 1) * 32)) <= 32)
							{
							}
							else
							{
								pSprite.move(-100 * Time.asSeconds(), 0);
							}
						}
						else
						{
							pSprite.move(-100 * Time.asSeconds(), 0);
						}*/

//collisionCheckRight

						/*collisionDeciderX1 = (pSprite.getPosition().x / 32) + 1;
						collisionDeciderY1 = pSprite.getPosition().y / 32;
						collisionCalcY = pSprite.getPosition().y - floor(pSprite.getPosition().y / 32) * 32;
						if(map[collisionDeciderX1][collisionDeciderY1] == 2)
						{
							if(collisionCalcY >= 20)
							{
								collisionDeciderY1 = ceil(pSprite.getPosition().y / 32);
							}
							else
							{
								collisionDeciderY1 = floor(pSprite.getPosition().y / 32);
							}
						}
						else
						{
							if(collisionCalcY >= 4)
							{
								collisionDeciderY1 = ceil(pSprite.getPosition().y / 32);
							}
							else
							{
								collisionDeciderY1 = floor(pSprite.getPosition().y / 32);
							}
						}

						if(map[collisionDeciderX1][collisionDeciderY1] == 2)
						{
							if((pSprite.getPosition().x - ((collisionDeciderX1 - 1) * 32)) <= 0.1 || (pSprite.getPosition().x - ((collisionDeciderX1 - 1) * 32)) <= 32)
							{
							}
							else
							{
								pSprite.move(100 * Time.asSeconds(), 0);
							}
						}
						else
						{
							pSprite.move(100 * Time.asSeconds(), 0);
						}*/

//movingView

						/*viewPosition.x = pSprite.getPosition().x + 16 - (screenDimension.x / 2);
						viewPosition.y = pSprite.getPosition().y + 16 - (screenDimension.y / 2);

						if(viewPosition.x < 0)
						{
							viewPosition.x = 0;
						}
						if(viewPosition.y < 0)
						{
							viewPosition.y = 0;
						}

						view.reset(sf::FloatRect(viewPosition.x, viewPosition.y, screenDimension.x, screenDimension.y));

						Window.setView(view);*/

//playerAnimation

						/*frameCounter = frameCounter + frameSpeed * Clock.restart().asSeconds();
						if(frameCounter >= switchFrame)
						{
							frameCounter = 0;
							Source.x++;
							if(Source.x * 32 >= pTexture.getSize().x)
							{
								Source.x = 0;
							}
						}*/